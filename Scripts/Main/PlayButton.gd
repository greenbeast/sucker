extends TextureButton

var playing = false
var play_button = preload("res://Assetts/Icons/forward.png")
var pause_button = preload("res://Assetts/Icons/pause.png")

func _ready():
	set_normal_texture(play_button)
	set_pressed_texture(play_button)
	set_disabled_texture(play_button)
	set_focused_texture(play_button)

func _on_PlayButton_pressed():
	if playing == true:
		$"../Music".set_stream_paused(false)
	elif playing == false:
		$"../Music".set_stream_paused(true)

func _on_PlayButton_toggled(_button_pressed):
	if playing == false:
		playing = true
	elif playing == true:
		playing = false
		
	if playing == false:
		set_normal_texture(play_button)
		set_pressed_texture(play_button)
		set_disabled_texture(play_button)
		set_focused_texture(play_button)
		set_hover_texture(play_button)
	else:
		set_normal_texture(pause_button)
		set_pressed_texture(pause_button)
		set_disabled_texture(pause_button)
		set_focused_texture(pause_button)
		set_hover_texture(pause_button)
