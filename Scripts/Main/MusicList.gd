extends ItemList

#var save_path = "user://saved_music.json"
var save_path = "user://saved_music.txt"
var player_data = []

func load_music():
	var file = File.new()
	var dir = Directory.new()
	
	if not file.file_exists(save_path):
		return
		
	if not dir.dir_exists("res://Music/"):
		return
		
	file.open(save_path, file.READ)
	var index = 1
	
	while not file.eof_reached():
		var line = file.get_line()
		#print(line + str(index))
		player_data.append(line)
	file.close()
	
	for i in len(player_data):
		var music_list = player_data[i]
		self.add_item(music_list)
		self.set_item_text(i, "      "+music_list.substr(0,len(music_list)-4))

func _ready():
	load_music()

func _on_MusicList_item_selected(index):
	var chosen_music = self.get_item_text(index)
	var music = $"../../Music"
	var music_dir = "res://Music/"
	var song_path = "res://Music/" + str(chosen_music) 
	var selected_music = Directory.new()
	var files = []
	
	selected_music.open(music_dir)
	selected_music.list_dir_begin()
	while true:
		var file = selected_music.get_next()
		#if file.starts_with(index):
		#	print("called")
		if file.ends_with(".mp3") or file.ends_with(".ogg") or file.ends_with(".wav"):
			files.append(file)
		elif file == "":
			break
	#print(selected_music)
	#music.play(chosen_music)
	#load the music then play it
	#This will be used to choose what music to play!
