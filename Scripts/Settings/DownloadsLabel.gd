extends RichTextLabel

func _ready():
	var path = OS.get_system_dir(OS.SYSTEM_DIR_DOWNLOADS)
	self.set_bbcode("[center][b]Downloads[/b] \n"+str(path)+"[/center]")
