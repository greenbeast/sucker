extends FileDialog

"""
UNFORTUNATELY IT SEEMS LIKE THE APP NEEDS TO CLOSE THEN RE-OPEN TO 
ACTUALLY IMPORT THE MUSIC INTO THE FILES
"""

#var save_path = "user://saved_music.json"
var save_path = "user://saved_music.txt"
var saved_music = []
var player_data = {}

func _ready():
	var path = OS.get_system_dir(OS.SYSTEM_DIR_DOWNLOADS)
	#Need to change, this is just for testing
	set_current_path(path)

func save_music():
	var file = File.new()
	#file.open(save_path, file.READ)
	#var content = file.get_as_text()
	#print(content)
	#file.READ_WRITE is the correct function here
	file.open(save_path, file.READ_WRITE)
	read_files()
	file.seek_end()
	for i in len(saved_music):
		file.store_line(saved_music[i])
	#print(saved_music)
	read_files()
	file.close()

func read_files():
	var file = File.new()
	file.open(save_path, file.READ)
	var contents = file.get_as_text()
	print(contents)

func _on_FileDialog_dir_selected(dir):
	#var audio = $"../AudioStreamPlayer2D"
	var selected_music = Directory.new()
	var music_path = "res://Music/"
	var files = []
	#$"../AudioStreamPlayer2D".play(path)
	#$AudioStreamPlayer2D.stream = load(path)
	"""
	Needs to copy the files over to the res:// directory
	for audio to be playable.
	"""
	var test_name = "full.wav"
	selected_music.open(dir)
	selected_music.list_dir_begin()
	while true:
		var file = selected_music.get_next()
		if file.ends_with(".mp3") or file.ends_with(".ogg") or file.ends_with(".wav"):
			files.append(file)
			Global.user_music.append(file)
			#print("song added "+file)
		elif file == "":
			break
	
	for i in len(files):
		if files[i] == test_name:
			pass
		else:
			var new_dir = str(dir+"/"+files[i])
			var new_music_path = str(music_path+files[i])
			#print(new_music_path)
			selected_music.copy(new_dir,new_music_path)
			#print(new_dir)
			
	saved_music = Global.user_music
	
	save_music()

