extends RichTextLabel

func _ready():
	var path = OS.get_system_dir(OS.SYSTEM_DIR_MUSIC)
	self.set_bbcode("[center][b]Music[/b] \n"+str(path)+"[/center]")
